const tabsCaption = document.querySelectorAll('.tabs__caption li');
for (let i = 0; i < tabsCaption.length; i++) {
    tabsCaption[i].addEventListener('click', function () {
        this.classList.add('active');
        let sibling = this.previousElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.previousElementSibling;
        }
        sibling = this.nextElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.nextElementSibling
        }
        const tabContentId = this.dataset.tabs;

        const tabContent = document.getElementById(tabContentId);
        const tabsContainer = tabContent.parentElement;
        const allTabsContent = tabsContainer.children;
        for (let i = 0; i < allTabsContent.length; i++) {
            allTabsContent[i].classList.remove('active');
        }

        tabContent.classList.add('active');
    })
}

$('.tabs__caption__tabs-2 li').click(function () {
    $('.tabs__caption__tabs-2 li').removeClass('active');
    $(this).addClass('active');

    const graphicDesign = $(this).attr('data-tabs');
    if (graphicDesign !== 'graphic_tab-all') {
        $('.graphic_imgs[data-tabs != ' + graphicDesign + ']')
            .addClass('active');

        $('.graphic_imgs[data-tabs = ' + graphicDesign + ']')
            .removeClass('active');
    } else {
        $('.graphic_imgs').removeClass('active')
    }
});

$('.button__graphic-design').click(function () {
    $('.button__graphic-design').addClass('invisible');
    $('.graphic__design').append(`<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">
<img class="graphic_imgs" data-tabs="graphic_tab-1" src="img/graphic%20design/graphic-design2.jpg" alt="img">`)
});

$('.slider_for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".slider_nav"
});

$('.slider_nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    centerPadding: 0,
    focusOnSelect: true,
    centerMode: true,
    asNavFor: ".slider_for"
});

$('.slider_for').on("afterChange", function (event, slick, currentSlider, nextSlider) {
    $('.text_for_section_for').removeClass('active').eq(currentSlider).addClass('active');
    $('.name_for_section_for').removeClass('active').eq(currentSlider).addClass('active');
    $('.profession_for_section_for').removeClass('active').eq(currentSlider).addClass('active');
    $('.img_for_section_for').removeClass('active').eq(nextSlider).addClass('active')


});
